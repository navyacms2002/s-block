**Industry solutions comparative study**
- 
**| Parameters | Amazon Rekognition | Microsft Recognitive Services |**

| Size of image | 5 MB per image  | 4MB per image |

| Result | supports injection directly from S3 but there was no major improvement in performance. Using S3 links could potentially save outgoing bandwidth costs, and using S3 allows us to use much larger files (15Mb). | Microsoft showed reasonable performance with some higher times on high load. |

| Study of dog picture | Animal (92%), Canine (92%), Dog (92%), Golden Retriever (92%), Mammal (92%), Pet (92%), Collie (51%) | Dog (99%), Floor (91%), Animal (90%), Indoor (90%), Brown (88%), Mammal (71%), Tan (27%), Starting (18%) |

| Output Response | **Bounding box** – The coordinates of the bounding box that surrounds the face. **Confidence** – The level of confidence that the bounding box contains a face. **Facial landmarks** – An array of facial landmarks. For each landmark (such as the left eye, right eye, and mouth), the response provides the x and y coordinates.
**Facial attributes** – A set of facial attributes, such as whether the face has a beard. For each such attribute, the response provides a value. 
**Quality** – Describes the brightness and the sharpness of the face. 
**Pose** – Describes the rotation of the face inside the image.
**Emotions** – A set of emotions with confidence in the analysis. | Face detection that perceives **faces and attributes** in an image; person **identification** that matches an individual in your private repository of up to 1 million people; perceived **emotion recognition** that detects a range of facial expressions like happiness, contempt, neutrality and fear; and recognition and **grouping** of similar faces in images.


**Which is better?**
According to me, Amazon is better than Microsoft to work with. It's easier,more accurate.

*These results (below) are just examples from the internet*
**Output by Microsoft**
`JSON:
 [
  {
    "faceId": "d8c5e56c-9d9d-44c0-8f74-3ff294728246",
    "faceRectangle": {
      "top": 76,
      "left": 446,
      "width": 226,
      "height": 284
    },
    "faceAttributes": null,
    "faceLandmarks": null
  }
]`

**Output by Amazon**
`Expected output:
	Face (99.945602417%)
	  SAD : 14.6038293839%
	  HAPPY : 12.3668470383%
	  DISGUSTED : 3.81404161453%
	  Sharpness : 10.0
	  Brightness : 31.4071826935
	  Eyeglasses(False) : 99.990234375%
	  Sunglasses(False) : 99.9500656128%
	  Gender(Male) : 99.9291687012%
	  EyesOpen(True) : 99.9609146118%
	  Smile(False) : 99.8329467773%
	  MouthOpen(False) : 98.3746566772%
	  Mustache(False) : 98.7549591064%
	  Beard(False) : 92.758682251%`
	  
![dog](/uploads/d5fe59c9a8d2249810105e2b4a2089bb/dog.jpg)