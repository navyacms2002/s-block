**What is neural network?**

Artificial Neural Networks or connectionist systems are computing systems highly inspired by the biological neural networks that reside in human brain. The data structures and functionality of neural networks are designed to simulate associative memory. Basically, it’s a computational model. Although, the structure of the ANN is affected by a flow of information. Hence, neural network changes were based on input and output. 

It's a technique for building a computer program that learns from data. It is based very loosely on how we think the human brain works.
A neural network consists of three important layers:

**Input Layer:** As the name suggests, this layer accepts all the inputs provided by the programmer.

**Hidden Layer**: Between the input and the output layer is a set of layers known as Hidden layers. In this layer, computations are performed which result in the output.

**Output Layer**: The inputs go through a series of transformations via the hidden layer which finally results in the output that is delivered via this layer.
![neural_network](/uploads/8995e309096c1f1a1cc5f0d2d10d173e/neural_network.jpg)

**What is Gradient Descent?**

Gradient descent is an optimization algorithm used to find the values of parameters (coefficients) of a function (f) that minimizes a cost function (cost). Gradient descent used when the parameters cannot be calculated analytically (e.g. using linear algebra). It's based on a convex function and twists its parameters iteratively to minimize a given function to its local minimum.
We take an input and if slope is positive, we shift the point to left and if slope is negative then shift right.
Doing iteratively we find local minimum. Calculus helps us determine which gradient slope will be apt for descent and ascent of slope.
![gradient-descent-convex-function](/uploads/0afc0ae957b87e4c830d61ff8f985f8a/gradient-descent-convex-function.jpg)

**How neural_networks learn?**

1. Each neuron has an activation function that defines the output of the neuron. 
2. When the data is passed through neurons, they apply their transformation to the information they receive from the neurons of the previous layer and sending it to the neurons of the next layer. When the data has crossed all the layers, the final layer will be give the result.
3. We will use a loss function to estimate the error and  measure how good or bad our prediction result was in relation to the correct result. Since we are using supervised learning, we have the correct outputs.
4. Once the loss has been calculated, this information is propagated backwards. Hence, it is called backpropagation.
5. Changes should be made so that the loss decreases and the model becomes better at giving the output.

**What is Backpropagation?**

Backpropagation is a method to alter the parameters (both weights and biases) of the neural network in the right direction. It starts by calculating the loss term first, and then the parameters of the neural network are adjusted in reverse order with an optimization algorithm taking into account this calculated loss. Given an artificial neural network and an error function, the method calculates the gradient of the error function with respect to the neural network's weights.

We perform backpropagation techniques for every training example, recording the desired changes and weights and biases and averaging those desired changes.
In the Stochastic Gradient Descent (SGD) the weights are updated after every training data.

