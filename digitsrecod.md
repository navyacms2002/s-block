**Keywords**

- **Import**  When we import a module, we are making it available to us in our current program as a separate namespace. 
 
- **cv2** OpenCV is the huge open-source library for the computer vision, machine learning, and image processing and now it plays a major role in real-time operation which is very important in today's systems. By using it, one can process images and videos to identify objects, faces, or even handwriting of a human. 

- **NumPy** is a library for the Python programming language, adding support for large, multi-dimensional arrays and matrices, along with a large collection of high-level mathematical functions to operate on these arrays. 

- **Pandas** pandas is a software library written for the Python programming language for data manipulation and analysis. In particular, it offers data structures and operations for manipulating numerical tables and time series. 

- **Matplotlib** Matplotlib is an amazing visualization library in Python for 2D plots of arrays. Matplotlib' function to show the plot is plt. show(). 

- **%matplotlib inline** sets the backend of matplotlib to the 'inline' backend.  With this backend, the output of plotting commands is displayed inline within frontends like the Jupyter notebook, directly below the code cell that produced it.

- **Seaborn** is a Python data visualization library based on matplotlib. It provides a high-level interface for drawing attractive and informative statistical graphics.  

- **Keras** is a powerful and easy-to-use free open source Python library for developing and evaluating deep learning models. It  allows you to define and train neural network models in just a few lines of code.

- **Scikit-learn** is a free software machine learning library for the Python programming language. It features various classification, regression and clustering algorithms including support vector machines. 

- **Convolutional Layers**. On a fully connected layer, each neuron's output will be a linear transformation of the previous layer, composed with a non-linear activation function (e.g., ReLu or Sigmoid).

- **Keras optimizer** provides the SGD class that implements the stochastic gradient descent optimizer with a learning rate and momentum. First, an instance of the class must be created and configured, then specified to the “optimizer” argument when calling the fit() function on the model.

- **Keras Image Data Generator** class actually works by accepting a batch of images used for training. ImageDataGenerator generates batches of image data with real-time data augmentation.

- **Flatten layer** A flatten layer collapses the spatial dimensions of the input into the channel dimension. 

- **2D max pooling block** moves a rectangle (window) over the incoming data, computing the maximum in each specific window. The size of the window is determined by the horizontal and vertical pooling factor and how big steps the window takes is determined by the horizontal and vertical stride.

- **Dropout approach** means that we randomly choose a certain number of nodes from the input and the hidden layers, which remain active and turn off the other nodes of these layers. After this we can train a part of our learn set with this network. 

- **Sigmoid**The logistic sigmoid function defined as (1/(1 + e^-x)) takes an input x of any real number and returns an output value in the range of -1 and 1 

- **RMSprop** is a gradient based optimization technique used in training neural networks. ... This normalization balances the step size (momentum), decreasing the step for large gradients to avoid exploding, and increasing the step for small gradients to avoid vanishing. 

- **ReLU activation function** ReLU stands for rectified linear unit, and is a type of activation function. Mathematically, it is defined as y = max(0, x). 

- **Kernel**  When a computation is done over a pixel neighborhood, it is common to represent this with a kernel matrix. This kernel describes how the pixels involved in the computation are combined in order to obtain the desired result. 

- **Softmax function** is often used in neural networks, to map the non-normalized output of a network to a probability distribution over predicted output classes. The softmax activation is normally applied to the very last layer in a neural net, instead of using ReLU, sigmoid, tanh, or another activation function. The reason why softmax is useful is because it converts the output of the last layer in your neural network into a probability distribution. 

- **Categorical Cross-Entropy loss.** Also called Softmax Loss. It is a Softmax activation plus a Cross-Entropy loss. If we use this loss, we will train a CNN to output a probability over the C classes for each image. It is used for multi-class classification. 
